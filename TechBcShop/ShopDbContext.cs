﻿using Microsoft.EntityFrameworkCore;
using TechBcShop.Web.Models;

namespace TechBcShop.Web
{
    public class ShopDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }

        public ShopDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasKey(c => c.Id);
            modelBuilder.Entity<Order>().HasKey(c => c.Id);
            modelBuilder.Entity<Product>().HasKey(c => c.Id);
            modelBuilder.Entity<OrderProduct>().HasKey(c => c.Id);

            modelBuilder.Entity<Product>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(s => s.CategoryId);

            modelBuilder.Entity<OrderProduct>()
                .HasOne(op => op.Order)
                .WithMany(o => o.OrderProducts)
                .HasForeignKey(p => p.OrderId);

            modelBuilder.Entity<OrderProduct>()
                .HasOne(op => op.Product)
                .WithMany(o => o.OrderProducts)
                .HasForeignKey(p => p.ProductId);

            modelBuilder.Entity<Category>()
                .Property(c => c.Name)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Category>()
                .Property(c => c.Description)
                .HasMaxLength(255);

            modelBuilder.Entity<Product>()
                .Property(c => c.Name)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Product>()
               .Property(c => c.Price)
               .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(c => c.CustomerName)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(c => c.Zip)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(c => c.Address)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(c => c.Isshipped)
                .HasDefaultValue(false);

            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Smartphones", Description = "smartphones such as an iPhone" },
                new Category { Id = 2, Name = "Tablets", Description = "tablets such as an iPad" },
                new Category { Id = 3, Name = "Cars", Description = "cars such as a Tesla" },
                new Category { Id = 4, Name = "Sports", Description = "sport goods" },
                new Category { Id = 5, Name = "Health", Description = "pils, drugs etc" },
                new Category { Id = 6, Name = "Slaves", Description = "people, animals etc" });

            modelBuilder.Entity<Product>().HasData(
                new Product { Id = 1, Name = "iPhone XS", Price = 2499, CategoryId = 1 },
                new Product { Id = 2, Name = "Xiaomi Redmi", Price = 10, CategoryId = 1 },
                new Product { Id = 3, Name = "Meizu Super XL", Price = 15, CategoryId = 1 },
                new Product { Id = 4, Name = "iPad Pro", Price = 2400, CategoryId = 2 },
                new Product { Id = 5, Name = "Samsung Galaxy", Price = 1500, CategoryId = 2 },
                new Product { Id = 6, Name = "Honda Accord", Price = 24999, CategoryId = 3 },
                new Product { Id = 7, Name = "Tesla Model S", Price = 49999, CategoryId = 3 },
                new Product { Id = 8, Name = "Adidas Jacket", Price = 100, CategoryId = 4 },
                new Product { Id = 9, Name = "Modafinil 100", Price = 50, CategoryId = 5 },
                new Product { Id = 10, Name = "Ibuprofen", Price = 10, CategoryId = 5 },
                new Product { Id = 11, Name = "BoJack Horseman", Price = 100000, CategoryId = 6 });
        }
    }
}

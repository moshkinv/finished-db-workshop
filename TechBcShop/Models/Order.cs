﻿using System.Collections.Generic;

namespace TechBcShop.Web.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public bool Isshipped { get; set; }
        public List<OrderProduct> OrderProducts { get; set; }
    }
}
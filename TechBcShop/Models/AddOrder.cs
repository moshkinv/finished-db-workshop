﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechBcShop.DAL.Models;

namespace TechBcShop.Web.Models
{
    public class AddOrder
    {
        public Order Order { get; set; }
        public List<Dictionary<Product, int>> Products { get; set; }
    }
}
